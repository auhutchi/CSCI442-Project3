This is mySimulator Readme. 

- Name: 
	- Austin Hutchison

- List of files and Description:
	- "main.cpp" : This creates a simulation, runs it, then prints it
	- "./utils/parse.[h & cpp]" : This determines the user inputs
	- "./utils/sim.[h & cpp]" : This is the core struct, it holds all the info for a simulation
				    It also has built in functions for statistic calculations and printing
	- "./utils/FCFS.[h & cpp]" : This is the algoritm for FCFS, it schedules/runs a sim
	- "./utils/RR.[h & cpp]" : This is the algorithm for RR, it schedules/runs a sim
	- "./utils/pri.[h & cpp]" : This is the algoruthm for PRIORITY, it schedules/runs a sim
	- "./utils/custom.[h & cpp]" : This is the algorithm for CUSTOM, it schedules/runs a sim
	- "./info/process.[h & cpp]" : This is a struct containing all the parameters of a process
	- "./info/thread.[h & cpp]" : This is a struct containing all the paramters of a thread
	- "./info/burst.[h & cpp]" : This is a struct containing all the paramters of a burst
	- "./info/event.[h & cpp]" : This is a struct that containes events. Although my implementation reused the "arrival"
					events and simply changed it's paramters
	- "./info/verbose.[h & cpp]" : This is a struct that keeps record of all the "events"/history of the algorithms
	- "./info/procStat.h"		: This is a struct used for calculationg stats for the different types of threads

- Unusual Features: 
	- Nothing of note

- Hours Spent
	- Too much, 7 nights/days, probably at least  6 hours each one of those, so say 42+ hours. Might of had netflix on in the background though
	
- Short Essay
	- My custom algoritm is basically random. When threads arrive, they are assigned a random priority level bewteen 0 and 5.
	  Then placed in a approriate priority queue based on their assigned priority. Threads are dispatched from the highest
	  priority queue just like in the priority algorithm but they are only allowed to run for a set timeSlice before they are
	  preempted and put back in their priority queue.

	- This algorithm will allow for starvation for the unlucky threads. However, the algortithm is very fair since it's based
	  off a random assignment of priority, so no matter what type of thread it is, it has an equal chance of being ran sooner.
	  The metrics I tried to optimize was fairness.

- Other note worthy Info:
	- My algorithms aren't very modular and they all started with my FCFS algorith copied over. I wanted to make them modular but 
	  I was having a hard time tracking down seg faults and was short on time. So I didn't have time to make it more modular.
	  Maybe if I had more time. Also, comments are abit more sparce that what I usualy do, but should be adequate
