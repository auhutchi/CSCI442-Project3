#include "verbose.h"
#include "event.h"
#include <iostream>
#include <string>
#include "process.h"
#include <vector>
#include <queue>


using namespace std;

verbose createVerbose(unsigned theTime, Event* currentEvent, queue<Event*> selectionQueue) {
	
	// Create type conversions
	const char* PROCESS_TYPES[4] = {
  		"SYSTEM",
  		"INTERACTIVE",
  		"NORMAL",
  		"BATCH"
		};
	const char* EVENT_TYPES[8] {
  	   	"THREAD_ARRIVED",  		// A thread was created in the system.	
	 	"THREAD_DISPATCH_COMPLETED",	// A thread has finished dispatching. 
	  	"PROCESS_DISPATCH_COMPLETED",	// A process has finished dispatching
		"CPU_BURST_COMPLETED",		// A CPU burst has finished.
	 	"IO_BURST_COMPLETED",		// An IO burst has finished.
	  	"THREAD_COMPLETED",		// Like CPU_BURST_COMPLETED, but also represents that the thread has completely finished.
		"THREAD_PREEMPTED",		// A thread has been preempted before its time slice expired.
	  	"DISPATCHER_INVOKED"		// The OS needs to schedule a new thread to run, if one is available.
	};
	const char* THREAD_STATES[5] {		// Represents valid states in which threads / processes can exist.
 	 	"NEW",
  		"READY",
  		"RUNNING",
  		"BLOCKED",
  		"EXIT"
	};

	// Create/populate a verbose struct
	verbose event;
	event.time = theTime;
	event.eventType = EVENT_TYPES[currentEvent -> type];
	event.threadId = currentEvent -> thread -> id;
	event.pid = currentEvent -> thread -> process -> pid;
	event.procType = PROCESS_TYPES[currentEvent -> thread -> process -> type];
	event.previousState = THREAD_STATES[currentEvent -> thread -> previous_state];
	event.newState = THREAD_STATES[currentEvent -> thread -> current_state];
//	event.timeSliceSize = timeSliceSize;
	event.priorityThreadCount = selectionQueue.size();

	if (currentEvent -> type < 7) {
		event.transitioned = true;
	}
	if (currentEvent -> type == 6) {
		event.timeSlice = true;
	} else if (currentEvent -> type == 7) {
		event.priority = true;
	}

	return event;
}

void printVerbose(vector<verbose> hist){
	for (unsigned i = 0; i < hist.size(); i++) {
		cout 	<< "At time " << hist[i].time << ":" << endl
			<< "\t" << hist[i].eventType << endl
			<< "\tThread " << hist[i].threadId << " in process " << hist[i].pid << " [" << hist[i].procType << "]" << endl;
	
		if (hist[i].transitioned) {
			cout << "\tTransitioned from " << hist[i].previousState << " to " << hist[i].newState << endl;
		} else if (hist[i].priority) {
			cout << "\tSelected from " << hist[i].priorityThreadCount << " threads; will run to completion of burst" << endl;
		}// else if (hist[i].timeSlice) {
		//	 cout << "\tSelected from " << hist[i].priorityThreadCount << " threads; will run run for time Slice" << endl;
		//}
		cout << endl;
	}
}
