#pragma once
#include <vector>

struct procStat {

	unsigned totalCount = 0;
	double avgResponseTime = 0;
	double avgTurnaroundTime = 0;
	
	std::vector<int> resTimes;	// All the response times
	std::vector<int> turnTimes;	// All the turn times

};
