#pragma once
#include <vector>
#include <string>
#include "event.h"
#include <queue>


// This struct containts information for each event that has ran.
// The primary use of this info is to print it out when the verbose
// flag is set.
struct verbose {
	
	unsigned time;
	std::string eventType;
	unsigned threadId;
	unsigned pid;
	std::string procType;
	bool transitioned = false;
	std::string previousState;
	std::string newState;
	bool timeSlice = false;
	unsigned timeSliceSize;
	bool priority = false;
	unsigned priorityThreadCount;	
	
};

verbose createVerbose(unsigned theTime, Event* currentEvent, std::queue<Event*> selectionQueue);

void printVerbose(std::vector<verbose> hist);
