#pragma once
#include "thread.h"



// Represents the various types of events that can happen in the simulation.

struct Event {
  
	// The possible types of the event.
	enum Type {
  
	   	THREAD_ARRIVED,  		// A thread was created in the system.	
	 	THREAD_DISPATCH_COMPLETED,	// A thread has finished dispatching. 
	  	PROCESS_DISPATCH_COMPLETED,	// A process has finished dispatching
		CPU_BURST_COMPLETED,		// A CPU burst has finished.
	 	IO_BURST_COMPLETED,		// An IO burst has finished.
	  	THREAD_COMPLETED,		// Like CPU_BURST_COMPLETED, but also represents that the thread has completely finished.
		THREAD_PREEMPTED,		// A thread has been preempted before its time slice expired.
	  	DISPATCHER_INVOKED		// The OS needs to schedule a new thread to run, if one is available.
	};

	Type type;			// The type of event.

	int time;			// The time at which the event occurs.

	Thread* thread;			// The thread for which the event applies (may be null).

	Event(Type type, int time, Thread* thread) : type(type), time(time), thread(thread) {}
};




/**
 * Comparator for std::priority_queue to correctly order event pointers.
 *
 * The priority queue puts the 'greatest' element at the front, so smaller times
 * should be considered 'greater' for the purpose of this function.
 */
struct EventComparator {
  bool operator()(const Event* e1, const Event* e2) {
     return e1->time >= e2->time;
  }
};
