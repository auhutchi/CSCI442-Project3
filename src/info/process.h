#pragma once
#include "thread.h"
#include <vector>




/**
 * Represents a simple process, which encapsulates one or more threads.
 */
struct Process {

	int pid;		// The ID of this process.
	
	enum Type {		// The different possible types for this process
  		SYSTEM,			// Priorty level 1 (Highest Prioirty)
  		INTERACTIVE,		// Priorty level 2	
  		NORMAL,			// Priorty level 3
  		BATCH			// Priorty level 4 (Lowest Prioirty)
	};
	
	Type type;		// The type of this process

	std::vector<Thread*> threads;// This processes threads 

	Process(int pid, Type type) : pid(pid), type(type) {}

};
