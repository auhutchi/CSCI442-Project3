/**
 * Contains the main() routine of what will eventually be your version of top.
 */

#include <cstdlib>
#include <ncurses.h>
#include <iostream>
#include "./utils/parse.h"
#include <stdlib.h>
#include <vector>
#include "./utils/sim.h"
#include "./info/process.h"
#include "./utils/FCFS.h"
#include "./utils/RR.h"
#include "./utils/pri.h"
#include "./utils/custom.h"

using namespace std;


/**
 * Entry point for the program.
 */
int main(int argc, char **argv) {

	// Get inputs
	vector<string> inputs = getcmdline(argc, argv);
	
	// Create a sim
	sim theSim = readSimFile(inputs);

	// Run the sim
	if (theSim.algorithm == "FCFS") {
		runFCFS(theSim);
	} else if (theSim.algorithm == "RR") {
		runRR(theSim);
	} else if (theSim.algorithm == "PRIORITY") {
		runPri(theSim);
	} else if (theSim.algorithm == "CUSTOM") {
		runCustom(theSim);
	}
	
	// Calculate sim stats
	theSim.calcStats();

	// Print the sim
	theSim.printSim();
	
}
