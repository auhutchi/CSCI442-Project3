#include <vector>
#include <getopt.h>
#include <iostream>
#include <cstdlib>
#include "parse.h"
#include <stdlib.h>
#include <string>
#include <vector>
using namespace std;

vector<string> getcmdline(int argc, char**argv) {

  vector<string> inputArgs(5,"0");

  // Alot of copy an past

   // An array of long-form options.
  static struct option long_options[] = {
    {"algorithm",required_argument, NULL, 'a'},
    {"per_thread",no_argument, NULL, 't'},
    {"verbose",	no_argument, NULL, 'v'},
    {"help", 	no_argument, NULL, 'h'},
   
    // Terminate the long_options array with an object containing all zeroes.
    {0, 0, 0, 0}
  };

  // getopt_long parses one argument at a time. Loop until it tells us that it's
  // all done (returns -1).
  while (true) {
    // getopt_long stores the latest option index here,you can get the flag's
    // long-form name by using something like long_options[option_index].name
    int option_index = 0;

    // Process the next command-line flag. the return value here is the
    // character or integer specified by the short / long options.
    int flag_char = getopt_long(
        argc,           // The total number of arguments passed to the binary
        argv,           // The arguments passed to the binary
        "tvha:",     // Short-form flag options
        long_options,   // Long-form flag options
        &option_index); // The index of the latest long-form flag

    // Detect the end of the options.
    if (flag_char == -1) {
      break;
    }

    switch (flag_char) {
    case 0:
      cout << "Saw --" << long_options[option_index].name << " flag" << endl;
      break;

    case 'a':
      		inputArgs[0] = optarg;
      break;

    case 't':
      		inputArgs[1] = "1";
      break;
    case 'v':
		inputArgs[2] = "1";	
	break;
    case 'h':
      		inputArgs[3] = "1";
      break;

     case '?':
      // This represents an error case, but getopt_long already printed an error
      // message for us.
      break;

    default:
      // This would only happen if a flag hasn't been handled, or if you're not
      // detecting -1 (no more flags) correctly.
      exit(EXIT_FAILURE);
    }
  }


  // Print any other command line arguments that were not recognized as flags.
  if (optind < argc) {
	inputArgs[4] = argv[optind];
	if ((optind + 1) < argc) {
  		cout << "Non-option ARGV-elements: ";
    		for (int i = optind; i < argc; i++) {
      			cout << argv[i] << " ";
    		}
		    cout << endl;
	}
  }

  return inputArgs;
}
