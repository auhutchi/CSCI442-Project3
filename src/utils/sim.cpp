// sim.cpp
/*
	This facilitates the reading in of the simulation file and creating
	all the processes, threads, bursts, and the thread arrival events.
*/
#include "sim.h"
#include "../info/process.h"
#include "../info/thread.h"
#include "../info/burst.h"
#include "../info/event.h"
#include "../info/procStat.h"
#include "../info/verbose.h"
#include <vector>
#include <queue>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

sim readSimFile(vector<string> inFlags){
	
	// Create a sim
	sim thisSim;

	// Determine user flags
	if (inFlags[0] != "0") {
		thisSim.algorithm = inFlags[0];
	}
	if (inFlags[1] == "1") {
		thisSim.perThreadFlag = true;
	}
	if (inFlags[2] == "1") {
		thisSim.verboseFlag = true;
	}
	if (inFlags[3] == "1") {
		thisSim.helpFlag = true;
	}
	if (inFlags[4] != "0") {
		thisSim.simFileName = inFlags[4];
	}

	// Open simulation file
	ifstream simFile(thisSim.simFileName.c_str());

	// Check for error
	if (!simFile) {
		cerr << "Unable to read from " << thisSim.simFileName.c_str() << endl;
		exit(EXIT_FAILURE);
	}

	// Get the simulator params
	simFile >> thisSim.numProcs
		>> thisSim.threadOverhead
		>> thisSim.procOverhead;

	// Get Processes
	for (unsigned i = 0; i < thisSim.numProcs; i++) {
		
		// Get proc parms
		unsigned procId, procType, numThreads;
		simFile	>> procId
			>> procType
			>> numThreads;
		
		// Create a process
		Process* newProc = new Process(procId, (Process::Type) procType);
		
		// Push the proc onto the sim
		thisSim.procs.push_back(newProc);		

		// Get threads for this Proc
		for (unsigned j = 0; j < numThreads; j++) {
			
			// Get thread params
			unsigned arrivalTime, numBursts;
			simFile >> arrivalTime
				>> numBursts;

			// Create a thread
			Thread* newThread = new Thread;
		
			// Populate the thread with known data
			newThread -> id = j;
			newThread -> process = newProc;
			newThread -> arrival_time = arrivalTime;

			// Get Bursts from this thread
			for (unsigned k = 0; k < numBursts; k++) {
				
				// Get burst params
				unsigned cpuTime, ioTime;
				if ( k != numBursts-1) {
					simFile >> cpuTime
						>> ioTime;
				} else {
					simFile >> cpuTime;
					ioTime = 0;
				}

				// Create a cpu Burst and push it onto the newThread burst queue
				Burst* newCpuBurst = new Burst(Burst::CPU, cpuTime);
				newThread -> bursts.push(newCpuBurst);
				
				// If there was an IO burst, create it and push it onto the newThread Burst queue
				if (ioTime != 0){
					Burst* newIoBurst = new Burst(Burst::IO, ioTime);
					newThread -> bursts.push(newIoBurst);
				}
			}

			// Add thread to newProc threads vector
			newProc -> threads.push_back(newThread);
			
			// Add thread to eventArrival Queue
			thisSim.eventArrivals.push(new Event(Event::THREAD_ARRIVED, arrivalTime, newThread));
			
		}
		
	}
	return thisSim;	
}

void sim::calcStats() {
	// Calculate percents
	cpuUtil = (1-((idleTime * 100.00)/(elapsedTime * 100.00))) * 100.00;
	cpuEff = ((serviceTime * 100.00) /elapsedTime);

	// Calc ProcStats
	
	// resize procStats
	procStats.resize(4);
	
	// Get thread stats	
	for (unsigned i = 0; i < 4; i++){
		procStat pStat;
		for (unsigned j = 0; j  < procs.size(); j++) {
			for (unsigned k = 0; k < procs[j] -> threads.size(); k++) {
				if (procs[j] -> type == i) {
					// Get thread info
					int arrive, start, exit;
					arrive = procs[j] -> threads[k] -> arrival_time;
					start = procs[j] -> threads[k] -> start_time;
					exit = procs[j] -> threads[k] -> end_time;
					pStat.resTimes.push_back(start - arrive);
					pStat.turnTimes.push_back(exit - arrive);
				}
			}
		
		}
		procStats[i] = pStat;
	}

	// Calc avgs
	for (unsigned i = 0; i < 4; i++) {
		
		if (procStats[i].resTimes.size()) {	
			double resTotal = 0;
			double turnTotal = 0;
			for (unsigned j = 0; j < procStats[i].resTimes.size(); j++) {
				resTotal = resTotal + procStats[i].resTimes[j];
			}	
			for (unsigned k = 0; k < procStats[i].turnTimes.size(); k++) {
				turnTotal = turnTotal + procStats[i].turnTimes[k];
			}
			procStats[i].avgResponseTime = ((resTotal * 100.00) / procStats[i].resTimes.size()) / 100.00;
			procStats[i].avgTurnaroundTime = turnTotal / procStats[i].turnTimes.size();
			procStats[i].totalCount = procStats[i].resTimes.size();		
		}
	}
}

void sim::runSim() {
	
}

void sim::printHelp() {
	cout 	<< "Welcome to the unhelpful help" << endl
		<< "You chose to run this Simulation File: " << simFileName << endl
		<< "The current simulation algorithm is: " << algorithm << endl
		<< "To change this algorthim, set the '--algorithm' flag '-a' and the desired algorithm afterwards" << endl
		<< "The avaliable algorithms are 'FCFS', 'RR', 'PRIORITY', 'CUSTOM'" << endl
		<< "Please read documentation for futher info on the algorithms" << endl<<endl
		<< "To see the history of of each event, set the '--verbose' flag '-v'"	<< endl
		<< "To see the perThread statistics, set the '--per_thread' flag '-t' "	<< endl << endl;
}

void sim::printPerThread() {

	// Create type conversions
	const char* PROCESS_TYPES[4] = {
  		"SYSTEM",
  		"INTERACTIVE",
  		"NORMAL",
  		"BATCH"
		};

	for (unsigned i = 0; i < procs.size(); i++) {
		cout << "Process " << procs[i] -> pid << " [" << PROCESS_TYPES[procs[i] -> type] << "]:" << endl;
		for (unsigned j = 0; j < procs[i] -> threads.size(); j++) {
			cout 	<< "  \tThread " 	<< procs[i] -> threads[j] -> id << ":" 
				<< "  \tARR: " 	<< procs[i] -> threads[j] -> arrival_time
				<< "  \tCPU: "	<< procs[i] -> threads[j] -> service_time
				<< "  \tI/O: "	<< procs[i] -> threads[j] -> io_time
				<< "  \tTRT: "	<< procs[i] -> threads[j] -> end_time - procs[i] -> threads[j] -> arrival_time
				<< "  \tEND: "	<< procs[i] -> threads[j] -> end_time
				<< endl;
		}
	}
}

void sim::printSim() {
		
	if (helpFlag) {
		printHelp();
	}

	if (verboseFlag) {
		printVerbose(history);
	}

	if (perThreadFlag) {
		printPerThread();
	}

	// Create type conversions
	const char* PROCESS_TYPES[4] = {
  		"SYSTEM",
  		"INTERACTIVE",
  		"NORMAL",
  		"BATCH"
		};
	if (procStats.size() > 3) {
	// Print thread stata
	for (unsigned i = 0; i < 4; i++) {
		cout 	<< PROCESS_TYPES[i] << " THREADS:" << endl
			<< "\tTotal count:\t\t" << procStats[i].totalCount << endl
			<< "\tAvg response time:\t" << procStats[i].avgResponseTime << endl
			<< "\tAvg turnaround time:\t" << procStats[i].avgTurnaroundTime << endl << endl;
	}
	}

	// print total stats
	cout 	<< "Total elapsed time:\t" 	<< elapsedTime << endl
		<< "Total service time:\t" 	<< serviceTime << endl
		<< "Total I/O time:\t\t"	<< ioTime << endl
		<< "Total dispatch time:\t"	<< dispatchTime << endl
		<< "Total idle time:\t"		<< idleTime << endl
		<< endl
		<< "CPU utilization:\t"		<< cpuUtil << "%" << endl
		<< "CPU efficiency:\t\t"	<< cpuEff << "%" << endl
		<< endl;	
}

