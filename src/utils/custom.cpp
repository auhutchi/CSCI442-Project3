#include "custom.h"
#include "sim.h"
#include "../info/event.h"
#include <queue>
#include <vector>
#include "../info/verbose.h"
#include "../info/thread.h"
#include "../info/burst.h"
#include "../info/process.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

void runCustom(sim &theSim) {

	// Setup Initial Conditions
	Event* firstEvent = theSim.eventArrivals.top();
	unsigned timeSlice = theSim.timeSliceSize;
	int currentTime = 0;
	int dispatchTimeLeft = 0;
	int runTimeLeft = 0;
	int nextEventArrival = firstEvent -> time;
	int nextBlockedTime = -1;
	bool stuffToDo = true;		// Turns false when everything has ran to completion
	//unsigned lastThread = 999;	// Stores the last thread id running in the cpu
	int lastProc = 999;	//firstEvent -> thread -> process -> pid;	// Stores the last process id running in the cpu
	bool preEmpted = false;
	bool procDispatch = false;
	bool dispatching = false;
	bool running = false;
	bool moreEvents = !theSim.eventArrivals.empty();
	vector<queue<Event*>> priQueue;
	priQueue.resize(6);
	priority_queue<Event*, vector<Event*>, EventComparator> blockedQueue;
	Event* currentEvent;
	bool newEvent = true;
	srand(clock());

	// Schedule/run using a FCFS algoritm
	while (stuffToDo) {
		
		// Handle Blocked events
		while ((currentTime == nextBlockedTime) && (!blockedQueue.empty())) {		// Schedule all events arriving at the current time
	
			// Get the event
			Event* readyEvent = blockedQueue.top();
			blockedQueue.pop();
			nextBlockedTime = -1;
			
			readyEvent -> thread -> bursts.pop();
			
			// Schedule it
			priQueue[readyEvent->thread->priority].push(readyEvent);
			
			// Change thread states
			readyEvent -> thread -> previous_state = readyEvent -> thread -> current_state;
			readyEvent -> thread -> current_state = (Thread::State) 1;	// Ready

			// Change event type
			readyEvent -> type = (Event::Type) 4;	// Thread completed

			// Add it to history for verbose
			verbose eventArrival = createVerbose(currentTime, readyEvent, priQueue[readyEvent->thread->priority]);
			theSim.history.push_back(eventArrival);

			// Get next arrival time
			if (!blockedQueue.empty()) {
				Event* nextEvent = blockedQueue.top();
				nextBlockedTime = nextEvent -> time;
			}
		}

		// Handle event arrivals
		while ((currentTime == nextEventArrival) && (moreEvents)) {		// Schedule all events arriving at the current time
	
			// Get the event
			Event* readyEvent = theSim.eventArrivals.top();
			theSim.eventArrivals.pop();
			
			readyEvent->thread->priority = rand() % 6;
			
			// Schedule it
			priQueue[readyEvent->thread->priority].push(readyEvent);
			

			// Change thread states
			readyEvent -> thread -> previous_state = readyEvent -> thread -> current_state;
			readyEvent -> thread -> current_state = (Thread::State) 1;	// Ready

			// Update thread with arrival time
			readyEvent -> thread -> arrival_time = currentTime;

			// Add it to history for verbose
			verbose eventArrival = createVerbose(currentTime, readyEvent, priQueue[readyEvent->thread->priority]);
			theSim.history.push_back(eventArrival);

			// Get next arrival time
			if (theSim.eventArrivals.empty()) {
				moreEvents = false;
			} else {
				Event* nextEvent = theSim.eventArrivals.top();
				nextEventArrival = nextEvent -> time;
			}
		}
		procFinishedJump:
		bool emptyPriQueue = true;
		for (unsigned i = 0; i < priQueue.size(); i++) {
			if (priQueue[i].size()) {
				emptyPriQueue = false;
				break;
			}
		}
		if (!emptyPriQueue){	
		
		// Get the current event we care about
		if (newEvent) {
			for (unsigned i = 0; i < priQueue.size(); i++) {
				if (priQueue[i].size()) {
					currentEvent = priQueue[i].front();
					newEvent = false;
					break;	
				}
			}
		}
			
		// If nothing is running, dispatch a thread
		if (!running && !dispatching) {
			

			currentEvent -> type = (Event::Type) 7;		// DISPATCHER_INVOKED
			
			// Change cpu to dispatching
			dispatching = true;
			running = false;

			// Set the amount of time it'll take to dispatch
			if (lastProc != currentEvent -> thread -> process -> pid) {
				dispatchTimeLeft = theSim.procOverhead;
				procDispatch = true;
			} else {
				dispatchTimeLeft = theSim.threadOverhead;
			}


			// Add it to histor for verbose
			verbose eventDispatched = createVerbose(currentTime, currentEvent, priQueue[currentEvent->thread->priority]);
			theSim.history.push_back(eventDispatched);
		}	

		// If done dispatching, set thread to running
		if (!running && (dispatchTimeLeft == 0) && dispatching) {
			if (procDispatch) {
				currentEvent -> type = (Event::Type) 2;	// Process_Dispatch_completed
				theSim.dispatchTime = theSim.dispatchTime + theSim.procOverhead;	// Add dispatch overhead to stats
			} else {
				currentEvent -> type = (Event::Type) 1; // Thread Dispatch Completed
				theSim.dispatchTime = theSim.dispatchTime + theSim.threadOverhead;	// Add dispatch overhead to stats
			}

			// Change cpu to running
			running = true;
			dispatching = false;
			procDispatch = false;

			// Change thread states
			currentEvent -> thread -> previous_state = currentEvent -> thread -> current_state;
			currentEvent -> thread -> current_state = (Thread::State) 2;	// Running

			// Add it to history for verbose
			verbose eventDispatchComp = createVerbose(currentTime, currentEvent, priQueue[currentEvent->thread->priority]);
			theSim.history.push_back(eventDispatchComp);

			// Set run time
			if (timeSlice < currentEvent -> thread -> bursts.front() -> length) {
				runTimeLeft = timeSlice;
				preEmpted = true;
				currentEvent -> thread -> bursts.front() -> length = currentEvent -> thread -> bursts.front() -> length - timeSlice;
			} else {
				runTimeLeft = currentEvent -> thread -> bursts.front() -> length;
			}

			// Add runtime to sims service time stats
			theSim.serviceTime = theSim.serviceTime + runTimeLeft;
			currentEvent -> thread -> service_time = currentEvent -> thread -> service_time + runTimeLeft;

			// Set start time if this is the first burst
			if (currentEvent -> thread -> start_time == -1) {
				currentEvent -> thread -> start_time = currentTime;
			}
		}

		// If done running
		if ((runTimeLeft == 0) && !dispatching && running) {
			

			
			// Change cpu to idle
			running = false;
			dispatching = false;

			if (!preEmpted){
				// Pop off the burst
				currentEvent -> thread -> bursts.pop();
			}

			// Handle if thread completes
			if(currentEvent -> thread -> bursts.empty() || preEmpted){
				
				// Change Thread states
				currentEvent -> thread -> previous_state = currentEvent -> thread -> current_state;


				// Change event type
				if (preEmpted) {
					currentEvent -> type = (Event::Type) 6;	// Thread preEmpted
					currentEvent -> thread -> current_state = (Thread::State) 1;	// Ready
					// Schedule it
					priQueue[currentEvent->thread->priority].push(currentEvent);
					preEmpted= false; 
				} else {
					currentEvent -> type = (Event::Type) 5;	// Thread completed
					currentEvent -> thread -> current_state = (Thread::State) 4;	// Exit
				// Record finish time
				currentEvent -> thread -> end_time = currentTime;
				}

				// Add it to history for verbose
				verbose eventComplete = createVerbose(currentTime, currentEvent, priQueue[currentEvent->thread->priority]);
				theSim.history.push_back(eventComplete);
							


			// Handle if thread becomes blocked
			} else if (currentEvent -> thread -> bursts.front() -> type == 1) {
				
				// Change the event time to when it is no longer blocked
				currentEvent -> time = currentEvent -> thread -> bursts.front() -> length + currentTime;
				
				// Add the io time to the io Stats
				theSim.ioTime = theSim.ioTime + currentEvent -> thread -> bursts.front() -> length;
				currentEvent -> thread -> io_time = currentEvent -> thread -> io_time + currentEvent -> thread -> bursts.front() -> length;

				// Push the event onto the blocked queue
				blockedQueue.push(currentEvent);
				nextBlockedTime = blockedQueue.top() -> time;

				// Change Thread states
				currentEvent -> thread -> previous_state = currentEvent -> thread -> current_state;
				currentEvent -> thread -> current_state = (Thread::State) 3;	// Blocked

				// Change event type
				currentEvent -> type = (Event::Type) 3;	// CPU Burst Complete
				
				// Add it to history for verbose
				verbose eventBlocked = createVerbose(currentTime, currentEvent, priQueue[currentEvent->thread->priority]);
				theSim.history.push_back(eventBlocked);
			
			} else {
				cout << "FINSIHED RUNNING ERROR CHANGING EVENT" << endl;
				cout << currentEvent -> thread -> bursts.front() -> type << endl;
				exit(EXIT_FAILURE);
			}
			lastProc = currentEvent -> thread -> process -> pid;	// Stores the last process id running in the cpu
 
			priQueue[currentEvent->thread->priority].pop();	
			newEvent = true;
			goto procFinishedJump;

		}
	
		}
		cout << currentTime << endl;
		currentTime++;
		if (dispatching) {
			dispatchTimeLeft--;
		} 
		if (running) {
			runTimeLeft--;
		}
		if (blockedQueue.empty() && theSim.eventArrivals.empty() && emptyPriQueue) {
			theSim.elapsedTime = currentTime - 1;
			break;
		}
		if (emptyPriQueue) {
			theSim.idleTime++;
		}
	}
}
