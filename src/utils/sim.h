#pragma once
#include <vector>
#include <queue>
#include <string>
#include "../info/event.h"
#include "../info/procStat.h"
#include "../info/verbose.h"

struct sim {

	// Simulator Paramters
	
		// Given by me
		int timeSliceSize = 3;		
	
		// Flags passed in by user
		bool perThreadFlag = false;
		bool verboseFlag = false;
		bool helpFlag = false;
		std::string  algorithm = "FCFS";
		std::string simFileName;
		
		// Given in the simultation file
		unsigned numProcs = 0;		// Number of processes in this simulation
		unsigned threadOverhead = 0;	// Thread Switch Overhead
		unsigned procOverhead = 0;	// Processor Switch Overhead
		
	// Simulator Stats
		std::vector<procStat> procStats;// Contains the stats for the different process types
		unsigned elapsedTime = 0;	// The total time required to execute all threads to completion
		unsigned serviceTime = 0;	// The time spent executing user processes (service time)
		unsigned ioTime = 0;		// The time spent performing I/O (sum of all I/O bursts)
		unsigned dispatchTime = 0;	// The time spent doing process and thread switches (dispatching overhead)
		unsigned idleTime = 0;		// The amount of time the CPU was idle
		double cpuUtil = 100.00;	// CPU utilization
		double cpuEff = 100.00;		// CPU efficiency

	// Create a vector that keeps track of all the proceccess and threads;
	std::vector<Process*> procs;
	
	// Create a queue that puts orders events in the time that they'll arrive.	
	std::priority_queue<Event*, std::vector<Event*>, EventComparator> eventArrivals;

	// Create a vector of scheduled queues, the vector element postion indicates priority determined by the scheduler. 
	std::vector<std::queue<Event*>> schldEvents;	

	// Create a vector that holds the history of the sim, used for verbose output
	std::vector<verbose> history;

	// Run the sim
	void runSim();

	// Calc stats
	void calcStats();

	// Print stats
	void printSim();

	// Print verbose info
	//void printVerbose();
	
	// Print perThread info
	void printPerThread();

	// Print help info
	void printHelp();
};

sim readSimFile(std::vector<std::string> inFlags);
